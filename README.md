Log Watcher allows you to use a local machine (probably a Mac or Linux desktop machine), to automatically monitor multiple remote targets logs.
It is particular useful to avoid the problem that if a remote machine loses outbound e-mail capability it won't be able to surface error logs to you via native tools (e.g. Composr CMS's Health Checks or error notifications).

To use Log Watcher, just create a config.xml that looks a bit like...

```
<config>
    <email>someone@example.com</email>

    <target label="Some site" uptime-url="https://www.example.com/" uptime-string="Example Domain">
        /usr/bin/scp -q whoever@example.com:~/www/data_custom/errorlog.php /dev/stdout
    </target>
</config>
```

The target commands can be any system command that will output the contents of the file your target should be monitoring. In this example I am using SCP, and assuming I have an SSH key configured to allow me to access the example.com machine.
Important note: As Log Watcher will run from Cron, it likely does not have the shell variables set up to connect to your SSH agent. If your private key is encrypted it therefore may fail to be able to connect using it.

Defining uptime details are optional. It is a very simple uptime checker, it doesn't deal with HTTP statuses, just looks for the given string in the URL output.

Then hook it up as a Cron task, something like...

`0,15,30,45 * * * * /usr/bin/php ~/log_watcher/cron.php`

Log Watcher assumes you have working e-mail on the local machine via PHP's `mail` command. And for that matter, it assumes you have PHP installed.

If you want to have certain lines ignored in your error log you can likely pipe through something like sed.
