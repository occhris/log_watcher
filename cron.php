<?php

/**
 * Log Watcher
 *
 * @package log_watcher
 * @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License 3.0
 */


ini_set('display_errors', '1');
ini_set('allow_url_fopen', '1');
error_reporting(E_ALL);
set_time_limit(0);

if (!function_exists('simplexml_load_file')) {
    throw new Exception('Simple XML needs to be installed');
}

if (!is_file(__DIR__ . '/config.xml')) {
    throw new Exception('Missing config.xml');
}

if (@file_get_contents('http://www.google.com/') == '') {
    exit(); // Likely the Internet connection is down
}

require_once(__DIR__ . '/lib/diff.php');
$renderer = new Text_Diff_Renderer_unified();

$config = @simplexml_load_file(__DIR__ . '/config.xml');
if ($config === null) {
    throw new Exception('Could not parse config.xml');
}

if (empty((array)$config->target)) {
    throw new Exception('No targets configured');
}

if (empty((string)$config->email)) {
    throw new Exception('No email address configured');
}
$email = (string)$config->email;

if (!file_exists(__DIR__ . '/previous')) {
    mkdir(__DIR__ . '/previous');
}

$targets = array();

foreach ($config->target as $target) {
    $command = empty((string)$target) ? null : trim((string)$target);

    $label = empty((string)$target['label']) ? md5($command) : (string)$target['label'];

    if (isset($targets[$label])) {
        throw new Exception('Duplicate target label');
    }

    $uptime_url = empty((string)$target['uptime-url']) ? null : (string)$target['uptime-url'];
    $uptime_string = empty((string)$target['uptime-string']) ? null : (string)$target['uptime-string'];
    $max_attempts = empty((string)$target['max-attempts']) ? 1 : (int)$target['max-attempts'];

    $targets[$label] = array(
        'command' => $command,
        'uptime_url' => $uptime_url,
        'uptime_string' => $uptime_string,
        'max_attempts' => $max_attempts,
    );
}

$issues = array();

foreach ($targets as $label => $details) {
    if ($details['command'] !== null) {
        $previous_path = __DIR__ . '/previous/' . preg_replace('#[^\w]#', '_', $label) . '.dat';
        $old = is_file($previous_path) ? file_get_contents($previous_path) : null;

        $times = 0;
        do {
            $descriptorspec = array(
               0 => array('pipe', 'r'),
               1 => array('pipe', 'w'),
               2 => array('pipe', 'w')
            );

            $pipes = array();
            $proc = proc_open($details['command'], $descriptorspec, $pipes);
            stream_set_timeout($pipes[1], 5);
            stream_set_timeout($pipes[2], 5);
            $info = stream_get_meta_data($pipes[1]);
            $stdout = @strval(stream_get_contents($pipes[1]));
            /*if (empty($stdout)) {
                throw new Exception('Target command did not produce a result');
            }*/
            $stderr = @strval(stream_get_contents($pipes[2]));
            fclose($pipes[0]);
            fclose($pipes[1]);
            fclose($pipes[2]);
            proc_close($proc);

            $considered_an_error = (trim($stdout) == '') && ($details['max_attempts'] > 1); // If we are configured to re-try if blank, then blank reasonable implies there is an error

            $times++;
            $retrying = ($considered_an_error) && ($times < $details['max_attempts']);

            if ($retrying) {
                $issues[] = 'Re-trying ' . $label . ' @ ' . date('H:i:s') . '; ' . json_encode($info) . '; ' . $stderr;
                sleep(1);
            }
        }
        while ($retrying);

        $new = trim($stderr . "\n" . $stdout);

        if ($old !== null) {
            $diff = new Text_Diff(explode("\n", $old), explode("\n", $new));
            $diff_text = $rendered_diff = $renderer->render($diff);
            if ($diff_text != '') {
                $issue = $diff_text;
                if (!$considered_an_error) {
                    mail($email, 'Changes in error log for ' . $label, $issue);
                }

                $issues[] = $issue;
            }
        }

        if (!$considered_an_error) {
            if (@file_put_contents($previous_path, $new) === false) {
                throw new Exception('Cannot write to ' . $previous_path);
            }
        }
    }

    if (($details['uptime_url'] !== null) && ($details['uptime_string'] !== null)) {
        $page_contents = @file_get_contents($details['uptime_url']);
        if (stripos($page_contents, $details['uptime_string']) === false) {
            $issue = 'Could not find uptime string for ' . $details['uptime_url'];
            mail($email, 'Failed uptime check for ' . $label, $issue);

            $issues[] = $issue;
        }
    }
}

if (count($issues) != 0) {
    $log_file = fopen(__DIR__ . '/errors.log', 'at');

    ob_start();
    echo 'Found issues... ';
    var_dump($issues);

    if ($log_file !== false) {
        fwrite($log_file, date('Y-m-d H:i:s') . "\n");
        fwrite($log_file, ob_get_contents());
        fwrite($log_file, "\n");
        fclose($log_file);
    }

    ob_end_flush();
}
